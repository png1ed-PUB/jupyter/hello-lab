## Based on

https://github.com/binder-examples/jupyterlab

> You can point to a specific file using JupterLab
> by including a file path beginning with tree/
> to the end of urlpath, like so:
> ``?urlpath=lab/tree/path/to/my/notebook.ipynb``

- Binder: https://mybinder.org/v2/gh/binder-examples/jupyterlab/master?urlpath=lab/tree/index.ipynb